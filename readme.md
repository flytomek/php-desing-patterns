# PHP design patterns

## Usage

Run desired pattern via CLI, i.e. `php src/strategy.php`

## Available patterns

- strategy
- decorator
- value object
