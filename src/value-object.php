<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use App\ValueObject\Address;

$myAddress = new Address('Nowowiejska 25', 'Warsaw', '00-649', 'Poland');
$myWifesAddress = new Address('Nowowiejska 25', 'Warsaw', '00-649', 'Poland');

if($myAddress->isEqual($myWifesAddress)) {
    echo "Addresses are the same!\n";
}
