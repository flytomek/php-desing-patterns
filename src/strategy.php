<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use App\Strategy\Basket;
use App\Strategy\Payment\BitcoinPaymentStrategy;
use App\Strategy\Payment\PaypalPaymentStrategy;

$basket = new Basket();

$basket->setPaymentStrategy(new PaypalPaymentStrategy());
$basket->processPayment(100);

$basket->setPaymentStrategy(new BitcoinPaymentStrategy());
$basket->processPayment(200);
