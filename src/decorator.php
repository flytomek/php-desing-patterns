<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use App\Decorator\CarConfigurator\StandardCar;
use App\Decorator\CarConfigurator\Chassis\CeramicBrakes;
use App\Decorator\CarConfigurator\Engine\V8BiturboEngine;
use App\Decorator\CarConfigurator\Body\GoldColor;

$car = new StandardCar();

$car = new CeramicBrakes($car);
$car = new V8BiturboEngine($car);
$car = new GoldColor($car);

echo "Total price of the car is: " . $car->calculatePrice();

