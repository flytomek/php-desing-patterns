<?php

declare(strict_types=1);

namespace App\Decorator\CarConfigurator\Chassis;

use App\Decorator\CarConfigurator\CarConfigurationDecorator;

class CeramicBrakes extends CarConfigurationDecorator
{
    private const PRICE = 12000;

    public function calculatePrice(): int
    {
        echo "Applying " . self::PRICE . " (ceramic brakes) to the total price\n";
        return $this->configurator->calculatePrice() + self::PRICE;
    }
}
