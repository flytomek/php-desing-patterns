<?php

declare(strict_types=1);

namespace App\Decorator\CarConfigurator;

interface CarConfiguratorInterface
{
    public function calculatePrice();
}
