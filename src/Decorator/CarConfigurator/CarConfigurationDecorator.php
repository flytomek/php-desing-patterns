<?php

declare(strict_types=1);

namespace App\Decorator\CarConfigurator;

abstract class CarConfigurationDecorator implements CarConfiguratorInterface
{

    public function __construct(protected CarConfiguratorInterface $configurator)
    {
    }
}
