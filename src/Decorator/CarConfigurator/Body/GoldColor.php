<?php

declare(strict_types=1);

namespace App\Decorator\CarConfigurator\Body;

use App\Decorator\CarConfigurator\CarConfigurationDecorator;

class GoldColor extends CarConfigurationDecorator
{
    private const PRICE = 35000;

    public function calculatePrice(): int
    {
        echo "Applying " . self::PRICE . " (gold color) to the total price\n";
        return $this->configurator->calculatePrice() + self::PRICE;
    }
}
