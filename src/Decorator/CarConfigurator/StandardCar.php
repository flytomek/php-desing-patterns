<?php

declare(strict_types=1);

namespace App\Decorator\CarConfigurator;

class StandardCar implements CarConfiguratorInterface
{
    private const PRICE = 100000;

    public function calculatePrice(): int
    {
        echo "Applying " . self::PRICE . " (base price) to the total price\n";
        return self::PRICE;
    }
}
