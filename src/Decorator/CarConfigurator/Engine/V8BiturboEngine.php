<?php

declare(strict_types=1);

namespace App\Decorator\CarConfigurator\Engine;

use App\Decorator\CarConfigurator\CarConfigurationDecorator;

class V8BiturboEngine extends CarConfigurationDecorator
{
    private const PRICE = 47000;

    public function calculatePrice(): int
    {
        echo "Applying " . self::PRICE . " (V8 Biturbo engine) to the total price\n";
        return $this->configurator->calculatePrice() + self::PRICE;
    }
}
