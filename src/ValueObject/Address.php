<?php

declare(strict_types=1);

namespace App\ValueObject;

final class Address
{
    public function __construct(
        private readonly string $street,
        private readonly string $city,
        private readonly string $postalCode,
        private readonly string $country
    ) {
    }

    public function isEqual(Address $other): bool
    {
        return
            $this->street === $other->getStreet() &&
            $this->city === $other->getCity() &&
            $this->postalCode === $other->getPostalCode() &&
            $this->country === $other->getCountry()
        ;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function getCountry(): string
    {
        return $this->country;
    }
}
