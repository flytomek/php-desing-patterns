<?php

declare(strict_types=1);

namespace App\Strategy\Payment;

final class PaypalPaymentStrategy implements PaymentStrategyInterface
{
    public function pay($amount)
    {
        echo "Payment of {$amount} via Paypal\n";
    }
}
