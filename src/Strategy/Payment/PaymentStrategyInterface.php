<?php

declare(strict_types=1);

namespace App\Strategy\Payment;

interface PaymentStrategyInterface
{
    public function pay($amount);
}
