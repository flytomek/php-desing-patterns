<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Strategy\Payment\PaymentStrategyInterface;

final class Basket
{
    protected PaymentStrategyInterface $strategy;

    public function setPaymentStrategy(PaymentStrategyInterface $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function processPayment(int $amount): void
    {
        echo "Processing payment...\n";
        $this->strategy->pay($amount);
        echo "Payment completed!\n";
    }
}
